class CreateBarbers < ActiveRecord::Migration[7.0]
  def change

    create_table :barbers do |t|   
      t.text :name

      t.timestamps
    end

    Barber.create :name => 'Igor'
    Barber.create :name => 'Sasha'
    Barber.create :name => 'Ivan'
    Barber.create :name => 'Olga'

  end
end
